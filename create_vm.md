# Requirements
- A hypervisor such as VirtualBox, VMware, KVM, etc.
- The [FreeBSD Installer ISO](https://www.freebsd.org/where/) image.
    - Select the `amd64-disc1` version of the ISO file.

# Create the Virtual Machine
Creaate a Virtual Machine with the following settings:
- 1 CPU Core
- 1 GB RAM
- 4 GB Disk
    - If your hypervisor allows it, set the disk format to VHD (Virtual Hard Disk)

# Install FreeBSD
Follow these installation steps (to be refined):
- Keymap Selection: Continue with default keymap
- Hostname: freebsd-zfs (change this to whatever you like)
- Distribution Select: unselect all options
- Partitioning: Auto (ZFS)
- ZFS Configuration: 
    - Configuration Options: Leave default
    - Virtual Device type: stripe
    - Select the available disk
- Root password: set a password - we will randomize this later
- Setup ipv4 networking based on your local environment - we will override this later
- Time Zone Selector: 0 UTC
- System Configuration: Enable sshd and dumpdev
- System hardening: leave all blank
- Add user accounts: no
- Filan Configuration: Exit

Reboot into the newly installed system
